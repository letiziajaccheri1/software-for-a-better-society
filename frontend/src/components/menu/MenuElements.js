import styled        from 'styled-components'
import { Container } from '../globals'
import { Link }      from 'react-router-dom'

// Nav
export const Nav = styled.nav`
  position: relative;
  height: 80px;
  display: flex;
  flex-wrap: nowrap;
  align-items: center;
  justify-content: center;
  padding: 1.5rem 0 0.5rem;
  width: 100%;
`

// Nav container
export const NavContainer = styled(Container)`
  display: flex;
  flex-wrap: nowrap;
  align-items: center;
  justify-content: space-between;
  padding: 0;
`

// Logo
export const NavLogo = styled(Link)`
  color: black;
  cursor: pointer;
  justify-self: flex-start;
  display: flex;
  align-items: center;
  text-decoration: none;
  margin-right: 2rem;
  &:hover { color: inherit; }
  & img {
    width: 120px !important;
    height: auto;
  }
`

export const NavSpan = styled.span`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  font-weight: 650;
`

// Desktop menu
export const DesktopMenu = styled(NavSpan)`
  @media only screen and (max-width: 915px) {
    display: none;
  }
`

// Mobile menu
export const MobileMenu = styled(NavSpan)`
  @media only screen and (min-width: 916px) {
    display: none;
  }
`

// Hamburger Icon
export const HamburgerIcon = styled.img`
  margin: 4px;
  cursor: pointer;
  width: 25px;
  height: auto;
`

// Dropdown container
export const Dropdown = styled(Container)`
  position: relative;
  display: inline-block;
`

// Navlink
export const NavLink = styled(NavLogo)`
  position: relative;
  display: inline-block;
  height: fit-content;
  margin: 0.5rem;
  & h6 {
    font-size: 1rem;
    margin-bottom: 2px;
  }
  &::before {
    content: "";
    position: absolute;
    left: 0;
    bottom: 0;
    width: 0;
    height: 2px;
    background-color: black;
    background-image: linear-gradient(to right, black, black);
    transition: width .25s ease-out; }
  &:hover::before {
    width: 100%;
  }
  &.active::before {
    width: 100%;
  }
`

// Dropdown content
export const DropDownContent = styled.div.attrs(props => ({ id: props.id || "dropdown" }))`
  display: grid;
  opacity: 0;
  position: absolute;
  top: 100%;
  border: solid 1px lightgrey;
  border-right: none;
  border-left: none;
  width: 100%;
  padding: 12px 16px;
  z-index: 1;
  background-color: white;
  overflow-y: auto;
  height: auto;
  pointer-events: none;
  transition: opacity 0.2s ease-in-out;
  box-shadow: 0 2px 5px rgb(0, 0, 0, .1);
`