import styled from "styled-components";

export const Navigation = styled.div`
  display: inline-flex;
  flex-wrap: nowrap;
  align-items: center;
  /* background-color: #f9cfaa;  */
  background-color: #000;
  padding: 0 4px;
  color: white;
  font-size: calc(1rem - 3px);
  & a {
    text-decoration: none; 
    color: inherit;
    margin: 2px;
  }
  & a:hover {
    color: inherit;
  }
`