import React        from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { ScrollToTop } from './components/globals'
import { App }      from './components/globals'
import Home         from './pages/home'
import Menu         from './components/menu'
import Thesis       from './pages/phd'
import Footer       from './components/footer'
import TDT4290      from './pages/tdt4290'
import Master       from './pages/masterStudents'
import TDT10        from './pages/tdt10'
import Videos       from './pages/videos'
import Team         from './pages/team'
import Projects     from './pages/projects'
import Teaching     from './pages/teaching'
import Active       from './pages/projects/active'
import Completed    from './pages/projects/completed'
import Cooperators  from './pages/cooperators'
import              './App.css'

function Main() {
  React.useEffect(()=>{
    function scrollFunction() {
      const btn = document.getElementById("btn")
      if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
        btn.style.opacity = "100%"
        btn.style.pointerEvents = "inherit"
      } else {
        btn.style.opacity = "0"
        btn.style.pointerEvents = "none"
      }
    }
    window.addEventListener('scroll', scrollFunction)
  })

  // scroll to top 
  function topFunction() {
    document.body.scrollTop = 0
    document.documentElement.scrollTop = 0
  }

  return (
    <App>
      <Router> 
        <Menu/>
        <Switch>     
          <Route exact path='/'             component={Home}/>
          <Route exact path='/phd'          component={Thesis}/>
          <Route exact path='/teaching'     component={Teaching}/>
          <Route exact path='/active-projects'     component={Active}/>
          <Route exact path='/completed-projects'  component={Completed}/>
          <Route exact path='/team'         component={Team}/>
          <Route exact path='/projects'     component={Projects}/>
          <Route exact path='/tdt4290'      component={TDT4290}/>
          <Route exact path='/tdt10'        component={TDT10}/>
          <Route exact path='/master'       component={Master}/>
          <Route exact path='/videos'       component={Videos}/>
          <Route exact path='/cooperators'  component={Cooperators}/>
          <Route exact path='*'             component={Home}/>
        </Switch> 
        <Footer/>
        <ScrollToTop id='btn' onClick={topFunction}>&#8593;</ScrollToTop>
      </Router> 
    </App>
  )
}

export default Main