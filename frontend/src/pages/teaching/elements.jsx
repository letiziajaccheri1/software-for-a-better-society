import styled from "styled-components";
import { Button } from "../../components/globals";

export const Subject = styled.a`
  display: inline-block;
  width: 100%;
  min-width: 250px;
  cursor: pointer;
  text-decoration: none;
  color: inherit;
  margin: 2rem 0;

  & h3 {
    text-decoration: underline;
  }

  &:hover {
    color: inherit;
  }
`

export const CustomButton = styled(Button)`
  margin: 0;

  &:hover { background-color: #305fea; }
`

