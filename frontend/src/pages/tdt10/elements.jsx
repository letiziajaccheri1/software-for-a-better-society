import React from 'react';

// Syllabus themes list
export const UN5 = (
  <>
    <h5>Theme General about gender, UN Goal 5, the big picture.</h5>
    <ul>
      <li>Khaled Albusays , Pernille Bjørn, Laura Dabbish, Denae Ford, Emerson Murphy-Hill, Alexander Serebrenik, Margaret-Anne Storey, <a href="https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=9354402&tag=1" target="_blank" rel="noopener noreferrer">The Diversity Crisis in Software Development, </a> in IEEE Software, vol. 38, no. 2, pp. 19-25, March-April 2021, doi: 10.1109/MS.2020.3045817. (pages 7)</li>
      <li>Podcast by <a href="https://learning.acm.org/bytecast/ep12-denae-ford" target="_blank" rel="noopener noreferrer"> <strong> Denae Ford.</strong></a></li>
      <li>Jaccheri L, Pereira C, Fast S. <a href="https://arxiv.org/ftp/arxiv/papers/2102/2102.00188.pdf" target="_blank" rel="noopener noreferrer">Gender Issues in Computer Science: Lessons Learnt and Reflections for the Future. </a> In 2020 22nd International Symposium on Symbolic and Numeric Algorithms for Scientific Computing (SYNASC) 2020 Sep 1 (pp. 9-16). IEEE.</li>
      <li>Vinuesa, R., Azizpour, H., Leite, I., Balaam, M., Dignum, V., Domisch, S., … & Nerini, F. F. (2020). <a href="https://www.nature.com/articles/s41467-019-14108-y" target="_blank" rel="noopener noreferrer">The role of artificial intelligence in achieving the Sustainable Development Goals. </a> Nature communications, 11(1), 1-10.</li>
    </ul>
  </>
)

export const SchoolToUni = (
  <>
    <h5>Theme Education from School to University</h5>
    <ul>
      <li>Ozkaya, <a href="https://ieeexplore.ieee.org/document/9354391" target="_blank" rel="noopener noreferrer">Mom, Where Are the Girls?, </a> in IEEE Software, vol. 38, no. 2, pp. 3-6, March-April 2021, doi: 10.1109/MS.2020.3044410. (pages 4)</li>
      <li>R. Prado, W. Mendes, K. S. Gama and G. Pinto, <a href="https://www.computer.org/csdl/magazine/so/2021/02/09291061/1ptqJDWgLrW" target="_blank" rel="noopener noreferrer">How Trans-Inclusive Are Hackathons?, </a> in IEEE Software, vol. 38, no. 2, pp. 26-31, March-April 2021, doi: 10.1109/MS.2020.3044205. (pages 6)</li>
      <li>J. Simmonds, M. C. Bastarrica and N. Hitschfeld-Kahler, <a href="https://ieeexplore.ieee.org/document/9293108" target="_blank" rel="noopener noreferrer">Impact of Affirmative Action on Female Computer Science/Software Engineering Undergraduate Enrollment, </a> in IEEE Software, vol. 38, no. 2, pp. 32-37, March-April 2021, doi: 10.1109/MS.2020.3044841. (pages 6)</li>
      <li>E. Rubegni, M. Landoni, A. De Angeli, and L. Jaccheri, <a href="https://ntnuopen.ntnu.no/ntnu-xmlui/bitstream/handle/11250/2605847/genderNote.pdf?sequence=2&isAllowed=y" target="_blank" rel="noopener noreferrer">Detecting Gender Stereotypes in Children Digital StoryTelling, </a> in Proceedings of the Interaction Design and Children – IDC ’19, 2019, pp. 386–393, doi: 10.1145/3311927.3323156.</li>
      <li>Kshitij Sharma, Juan C. Torrado, Javier Gómez, Letizia Jaccheri, <a href="https://www.sciencedirect.com/science/article/pii/S1875952120300951" target="_blank" rel="noopener noreferrer">Improving girls’ perception of computer science as a viable career option through game playing and design: Lessons from a systematic literature review,</a> Entertainment Computing, Volume 36, 2021, 100387, ISSN 1875-9521, (<a href="https://www.sciencedirect.com/science/article/pii/S1875952120300951" target="_blank" rel="noopener noreferrer">link</a>) 13 pages.</li>
    </ul>
  </>
)

export const IndustrySociety = (
  <>
    <h5>Theme Industry and Society</h5>
    <ul>
      <li>L. S. Machado, C. Caldeira, M. Gattermann Perin and C. R. B. de Souza, <a href="https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=9268454" target="_blank" rel="noopener noreferrer">Gendered Experiences of Software Engineers During the COVID-19 Crisis, </a> in IEEE Software, vol. 38, no. 2, pp. 38-44, March-April 2021, doi: 10.1109/MS.2020.3040135.</li>
      <li>S. Zacchiroli, <a href="https://arxiv.org/pdf/2011.08488.pdf" target="_blank" rel="noopener noreferrer">Gender Differences in Public Code Contributions: A 50-Year Perspective, </a> in IEEE Software, vol. 38, no. 2, pp. 45-50, March-April 2021, doi: 10.1109/MS.2020.3038765.</li>
      <li>R. Nadri, G. Rodriguez-Perez and M. Nagappan, <a href="https://ieeexplore.ieee.org/document/9250363" target="_blank" rel="noopener noreferrer">Insights Into Nonmerged Pull Requests in GitHub: Is There Evidence of Bias Based on Perceptible Race?, </a> in IEEE Software, vol. 38, no. 2, pp. 51-57, March-April 2021, doi: 10.1109/MS.2020.3036758.</li>
      <li>L. B. Furtado, B. Cartaxo, C. Treude and G. Pinto, <a href="https://arxiv.org/pdf/2012.03716.pdf" target="_blank" rel="noopener noreferrer">How Successful Are Open Source Contributions From Countries With Different Levels of Human Development?, </a> in IEEE Software, vol. 38, no. 2, pp. 58-63, March-April 2021, doi: 10.1109/MS.2020.3044020.</li>
      <li>C. Pretorius, M. Razavian, K. Eling and F. Langerak, <a href="https://dl.acm.org/doi/10.1109/MS.2020.3043663" target="_blank" rel="noopener noreferrer">Combined Intuition and Rationality Increases Software Feature Novelty for Female Software Designers, </a> in IEEE Software, vol. 38, no. 2, pp. 64-69, March-April 2021, doi: 10.1109/MS.2020.3043663.</li>
      <li>J. Brevik, J. C. T. Vidal, and L. Jaccheri, <a href="https://dl.acm.org/doi/10.1145/3311927.3325322" target="_blank" rel="noopener noreferrer">Designing software to prevent child marriage globally, </a> in Proceedings of the Interaction Design and Children – IDC ’19, 2019, pp. 452–457, doi: 10.1145/3311927.3325322.</li>
    </ul>
  </>
)

// Resources list 
export const Resources = (
  <>
    <ul>
      <li><a href="https://unesdoc.unesco.org/ark:/48223/pf0000367416" target="_blank" rel="noopener noreferrer">I would blush if I could: closing gender divides in digital skills through education </a> – UNESCO Digital Library. (accessed Nov. 24, 2020), 146 pages.</li>
      <li><a href="https://ec.europa.eu/info/publications/she-figures-2018_en" target="_blank" rel="noopener noreferrer"> She Figures 2018, European Commission. </a> (accessed Nov. 24, 2020).</li>
      <li>S. Zweben and B. Bizot, Taulbee Survey Total Undergrad CS Enrollment Rises Again, but with Fewer New Majors; Doctoral Degree Production Recovers From Last Year’s Dip, 2020.</li>
      <li>L. L. Wang, G. Stanovsky, L. Weihs, and O. Etzioni, <a href="https://arxiv.org/abs/1906.07883" target="_blank" rel="noopener noreferrer">Gender trends in computer science authorship</a> , Jun. 2019.</li>
      <li><a href="https://www.informatics-europe.org/data/higher-education/ " target="_blank" rel="noopener noreferrer">Informatics Europe Higher Education Data Portal. </a>(accessed Dec. 09, 2020).</li>
      <li>L. J. Sax et al., Anatomy of an Enduring Gender Gap: The Evolution of Women’s Participation in Computer Science, J. Higher Educ., vol. 88, no. 2, pp. 258–293, Mar. 2017, doi: 10.1080/00221546.2016.1257306.</li>
      <li><a href="https://speakers.acm.org/speakers/jaccheri_10303" target="_blank" rel="noopener noreferrer"> ACM Destinguished Speaker Professor Letizia Jaccheri. </a> (accessed Nov. 24, 2020).</li>
      <li><a href="https://www.informatics-europe.org/activities/women-in-icst-research-and-education.html" target="_blank" rel="noopener noreferrer">Informatics Europe Women in Informatics Research & Education (WIRE). </a> (accessed Nov. 24, 2020).</li>
      <li><a href="https://ec.europa.eu/info/research-and-innovation/strategy/gender-equality-research-and-innovation_en" target="_blank" rel="noopener noreferrer">Gender equality in research and innovation | European Commission.</a> (accessed Dec. 09, 2020).</li>
      <li><a href="https://ec.europa.eu/digital-single-market/en/women-ict " target="_blank" rel="noopener noreferrer"> Women in Digital. Digital Single Market Policy.</a> (accessed Nov. 24, 2020).</li>
      <li>Gendered innovations – <a href="https://op.europa.eu/en/publication-detail/-/publication/d15a85d6-cd2d-4fbc-b998-42e53a73a449 " target="_blank" rel="noopener noreferrer"> Publications Office of the EU.  </a> (accessed Dec. 06, 2020).</li>
      <li><a href="https://www.ntnu.edu/girls" target="_blank" rel="noopener noreferrer"> The Girl Project Ada. </a> (accessed Nov. 24, 2020).</li>
      <li>ICT specialists,  <a href="https://ec.europa.eu/eurostat/documents/2995521/8115840/9-18072017-AP-EN.pdf" target="_blank" rel="noopener noreferrer"> More than 8 million ICT specialists employed in the EU in 2016, </a>  Jul. 18, 2016. (accessed Nov. 24, 2020).</li>
      <li>Bamberg CS30 Strategy. <a href="https://nachwuchs.wiai.uni-bamberg.de/ " target="_blank" rel="noopener noreferrer"> https://nachwuchs.wiai.uni-bamberg.de/ </a> (accessed Nov. 24, 2020).</li>
      <li>D. Lillis and S. McKeever, Minerva Award Submission: CS4All Initiative, 2018. [Online]. Available: <a href="https://www.informatics-europe.org." target="_blank" rel="noopener noreferrer"> here. </a> </li>
      <li><a href="https://www.tue.nl/en/working-at-tue/scientific-staff/irene-curie-fellowship" target="_blank" rel="noopener noreferrer"> Irène Curie Fellowship at TU Eindhoven. </a> (accessed Nov. 25, 2020).</li>
      <li>Genie | <a href="https://www.chalmers.se/en/about-chalmers/Chalmers-for-a-sustainable-future/initiatives-for-gender-equality/gender-initiative-for-excellence" target="_blank" rel="noopener noreferrer"> Gender Initiative for Excellence  </a> | Chalmers.  (accessed Nov. 25, 2020).</li>
      <li><a href="https://www.ntnu.edu/idun" target="_blank" rel="noopener noreferrer"> IDUN project at NTNU – from PhD to professor. </a> (accessed Nov. 25, 2020).</li>
      <li><a href="https://www.hmc.edu/about-hmc/2018/05/15/harvey-mudd-graduates-highest-ever-percentage-of-women-physics-and-computer-science-majors/" target="_blank" rel="noopener noreferrer"> Harvey Mudd Graduates Highest-ever Percentage of Women Physics and Computer Science Majors</a> | College News | Harvey Mudd College. (accessed Nov. 24, 2020).</li>
      <li>CMU’s <a href="https://www.cmu.edu/news/stories/archives/2016/september/undergrad-women-engineering-computer-science.html" target="_blank" rel="noopener noreferrer"> Proportion of Undergraduate Women in Computer Science and Engineering Soars Above National Averages </a> – News – Carnegie Mellon University.(accessed Nov. 24, 2020).</li>
      <li><a href="https://ghc.anitab.org/" target="_blank" rel="noopener noreferrer"> Grace Hopper Celebration, 2021. </a> (accessed Nov. 24, 2020).</li>
      <li><a href="https://womencourage.acm.org/2020/ " target="_blank" rel="noopener noreferrer"> womENcourage 2020 | ACM, 2020.  </a>(accessed Nov. 24, 2020).</li>
      <li><a href="http://ieee-wie-ilc.org/" target="_blank" rel="noopener noreferrer"> IEEE WIE International Leadership Conference, 2020. </a> (accessed Nov. 24, 2020).</li>
      <li><a href="https://acmweurope.acm.org/ " target="_blank" rel="noopener noreferrer"> Association for Computing Machinery | Supporting, celebrating and advocating for Women in Computing.  </a> (accessed Nov. 24, 2020).</li>
      <li><a href="http://www.ecwt.eu/en/home" target="_blank" rel="noopener noreferrer"> European Centre for Women and Technology (ECWT).  </a> (accessed Nov. 24, 2020).</li>
      <li><a href="https://ccs.org.cy/en/projects/cepis-women-in-ict-expert-group-13 " target="_blank" rel="noopener noreferrer"> Cyprus Computer Society » Project » CEPIS Women In ICT Expert Group. </a> (accessed Nov. 24, 2020).</li>
      <li><a href="https://www.itu.int/en/equals/Pages/default.aspx" target="_blank" rel="noopener noreferrer"> Global Partnership for Gender Equality in the Digital Age. </a> (accessed Nov. 24, 2020).</li>
      <li><a href="https://women4it.eu/" target="_blank" rel="noopener noreferrer"> Women4IT is a project aimed at developing innovative solutions to increase the numbers of vulnerable girls and young women in the European digital economy.</a> (accessed Nov. 24, 2020).</li>
      <li><a href="https://www.cost.eu/actions/CA19122" target="_blank" rel="noopener noreferrer"> COST Action CA19122 EUGAIN European Network For Gender. </a> (accessed Nov. 24, 2020).</li>
      <li><a href="https://www.eu-libra.eu/" target="_blank" rel="noopener noreferrer"> LIBRA | Unifying innovative efforts of European research centres to achieve gender equality in academia.</a> (accessed Dec. 09, 2020).</li>
      <li><a href="http://www.geecco-project.eu/home/ " target="_blank" rel="noopener noreferrer"> Geecco : Home. </a> (accessed Dec. 09, 2020).</li>
      <li>Plotina – <a href="https://www.plotina.eu/" target="_blank" rel="noopener noreferrer"> Promoting gender balance and inclusion in research, innovation and training. </a> (accessed Dec. 09, 2020).</li>
      <li><a href="https://genderaction.eu/ " target="_blank" rel="noopener noreferrer">Gender Action. </a>(accessed Dec. 09, 2020).</li>
      <li><a href="https://equal-ist.eu/" target="_blank" rel="noopener noreferrer"> EQUAL-IST | Gender Equality. </a> (accessed Nov. 24, 2020).</li>
      <li>Elsevier, <a href="https://www.elsevier.com/research-intelligence/resource-library/gender-report-2020 " target="_blank" rel="noopener noreferrer"> The researcher journey through a gender lens. </a> (accessed Nov. 24, 2020).</li>
      <li><a href="https://eige.europa.eu/gender-mainstreaming/toolkits/gear" target="_blank" rel="noopener noreferrer"> Gender Equality in Academia and Research – GEAR tool | EIGE.</a> (accessed Nov. 24, 2020).</li>
      <li><a href="https://ec.europa.eu/info/sites/default/files/research_and_innovation/strategy_on_research_and_innovation/documents/ec_rtd_gender-equality-factsheet.pdf" target="_blank" rel="noopener noreferrer"> GENDER EQUALITY A STRENGTHENED COMMITMENT IN HORIZON EUROPE </a></li>
    </ul>  
  </>
)