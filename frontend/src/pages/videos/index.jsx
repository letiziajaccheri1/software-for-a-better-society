import React from 'react';
import { Navigation } from '../../components/globals/Navigation';
import { VideoWrapper, VideosContainer } from './elements';
import { Wrapper, Row, H1, CustomWidthWrapper } from '../../components/globals';

export default function VideoSection() {
  return (
    <Wrapper>
      <Row>
        <CustomWidthWrapper>
          <Navigation><a href="/">home</a>&#47;<a href="/videos">videos</a></Navigation>
          <H1>Videos</H1>

          <VideosContainer>
            <VideoWrapper><iframe width="560" height="315" src="https://www.youtube.com/embed/Hv5R8Pa-YpY" title="Letizia Jaccheri - Presentation" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen /></VideoWrapper>
            <VideoWrapper><iframe width="560" height="315" src="https://www.youtube.com/embed/V7hzX9UgWeI" title="PEC ACM DSP Lectures - 2 | Prof. Letizia Jaccheri | Gender Issues in Computer Science" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen /></VideoWrapper>
            <VideoWrapper><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/hLa0wxeJlxo" title="Women in Science Day" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen /></VideoWrapper>
            <VideoWrapper><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/zM3ZXyIyYMU" title="Achieving agility and quality in product development - an empirical study of hardware startups" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen /></VideoWrapper>
            <VideoWrapper><iframe width="560" height="315" src="https://www.youtube.com/embed/Wxkc6820thU" title="International hackathon - Sustainable cities after Covid-19?" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen /></VideoWrapper>
          </VideosContainer>

        </CustomWidthWrapper>
      </Row>
    </Wrapper>
  )
}
