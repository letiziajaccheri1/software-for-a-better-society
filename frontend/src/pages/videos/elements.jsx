import styled from "styled-components";

export const VideoWrapper = styled.div`
  display: block;
  position: relative;
  width: 100%;
  max-width: 560px;
  & iframe {
    width: inherit;
    margin: 1rem 0;
  }
`

export const VideosContainer = styled.div`
  display: grid;
  gap: 1rem;
  grid-template-columns: repeat(2, 1fr);
  padding: 2rem 0;
  width: 100%;
  max-width: 990px;

  @media only screen and (max-width: 820px) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media only screen and (max-width: 558px) {
    grid-template-columns: repeat(1, 1fr);
  }
`