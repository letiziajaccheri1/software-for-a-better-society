import React from "react"

const cites = [
  {
    number: 1,
    cite: "Co-supervisor of PhD Patricia Lago, Politecnico di Torino, with Silvano Gai, theme - Software Engineering - 1993-1997" 
  },
  {
    number: 2,
    cite: "Member (Opponent) of the PhD judging committee for Dr. Sigurd Thunem, NTNU, theme - Software Engineering - 1997" 
  },
  {
    number: 3,
    cite: "Member (Opponent) of the PhD judging committee for Josep M. Ribo, Ph.D. program in Software of the Technical University of Catalunya, Barcelona, Spain – theme - Software Engineering - 2002" 
  },
  {
    number: 4,
    cite: "Member (Opponent) of the PhD judging committee for Sobah Abbas Petersen, at NTNU, theme - Information Systems, 2003" 
  },
  {
    number: 5,
    cite: "Co-Supervisor and member of the PhD judging committee for Ulrike Becker-Kornstadt, supervisor Dieter Rombach, Technische Universitaet Kaiserslautern, theme - Software Engineering - 2004" 
  },
  {
    number: 6,
    cite: "Co-Supervisor for Dr. Marco Torchiano with Reidar Conradi, theme - Software Engineering - 2001 - 2003" 
  },
  {
    number: 7,
    cite: "Co-Supervisor of PhD Mohagheghi Parastoo, theme - Software Engineering - 2001 - 2004" 
  },
  {
    number: 8,
    cite: "Administrator (Opponent) of the PhD judging commitee for Ekaterina Prasolova-Forland, at NTNU, theme - Technology Enhanced Learning - 2004" 
  },
  {
    number: 9,
    cite: "Member (Opponent) of the PhD judging committee for Raimundas Matulevicius, at NTNU, theme - Information Systems - 2005" 
  },
  {
    number: 10,
    cite: "Administrator (Opponent) of the PhD judging committee for Darijus Strasunskas, at NTNU, theme - Information Systems - 2006" 
  },
  {
    number: 11,
    cite: "Administrator (Opponent) of the PhD judging committee for Erlend Stav at NTNU - 2006" 
  },
  {
    number: 12,
    cite: "Co-supervisor for Jingyue Li, Components Off The Shelf Based Development, co-supervisor, theme - Software Engineering - 2002 - 2006" 
  },
  {
    number: 13,
    cite: "Administrator (Opponent) of the PhD judging committee for Jennifer Sampson at NTNU, theme - Information Systems - March 2007" 
  },
  {
    number: 14,
    cite: "Co-supervisor for Siv Hilde Houmb, Software Safety Modeling, theme - Software Engineering - 2002 - 2007" 
  },
  {
    number: 15,
    cite: "Administrator (Opponent) of the PhD judging committee for Yun Lin at NTNU – theme - Information Systems - April 2008" 
  },
  {
    number: 16,
    cite: "Member of the PhD judging committee (Opponent) for Juma Hemed Lungo, University of Oslo, theme - Information Systems - 2009" 
  },
  {
    number: 17,
    cite: "Co-Supervisor for Øyvind Hauge Knowledge Management for SPI, theme - Software Engineering - 2010" 
  },
  {
    number: 18,
    cite: "Co- supervisor for Zhou, Jianyun, Web-based system development, theme - Software Engineering - 2006"
  },
  {
    number: 19,
    cite: <p>Main supervisor for <a href="http://www.idi.ntnu.no/%7Ethomasos/"> Thomas Østerlie</a>, empirical software engineering and open source software with Reidar Conradi and Eric Monteiro, theme - Software Engineering - 2009</p>
  },
  {
    number: 20,
    cite: "Member of the PhD judging committee – Opponent and administrator for Birgit R. Krogstie, main supervisor Monica Divitini, theme - Technology Enhanced Learning - 2010"
  },
  {
    number: 21,
    cite: "Supervisor for ERCIM Dr. Anna Trifonova, theme - Art - 2007 - 2008"
  },
  {
    number: 22,
    cite: "Main supervisor for Salah Uddin Ahmed with Guttorm Sindre and Kristin Bergaust, theme - Art - 2006 – 2011"
  },
  {
    number: 23,
    cite: "Co-supervisor for Gasparas Jarulaitis, main supervisor Eric Monteiro – theme - Information Systems – 2006 - 2011"
  },
  {
    number: 24,
    cite: "Supervisor for ERCIM Dr. Jose Danado – theme ART - 2010 - 2011"
  },
  {
    number: 25,
    cite: "Member of the PhD judging committee – Opponent and administrator for Ole Andreas Alsos, Mobile Point-of-Care Systems in Hospitals: Designing for the Doctor-Patient Dialogue,  theme - Interaction Design - November 2011"
  },
  {
    number: 26,
    cite: "Co-supervisor for Ioannis Leftheriotis, first supervisor Konstantinos Chorianopoulos – theme - Interaction Design"
  },
  {
    number: 27,
    cite: "Co-supervisor for Guo Hong, with Hallvard Trætteberg and Alf Inge Wang – theme - Entertainment Computing – 2015"
  },
  {
    number: 28,
    cite: "Member (Opponent) of the PhD judging committee – Opponent and administrator for Vigdis Heimly, supervisor Eric Monteiro, opponents Christian Nøhr and Gunnar Hartvigsen – theme - Information Systems – 2012"
  },
  {
    number: 29,
    cite: "Co-supervisor for  Mohsen Anvari, with Reidar Conradi and Alf Inge Wang, theme - Software Engineering - 2015"
  },
  {
    number: 30,
    cite: "Supervisor for ERCIM Dr. Michail Giannakos – theme Creativity - 2012 - 2013"
  },
  {
    number: 31,
    cite: "Member (Opponent) of the PhD judging committee – Opponent and administrator for Wendy Mansilla supervisor Andrew Perkis – theme - Art - 2013"
  },
  {
    number: 32,
    cite: "Member (Opponent) of the PhD judging committee for Naoe Tatara Department of Computer Science Faculty of Science and Technology UiT – The Arctic University of Norway, Studying usage and experiences of mHealth technology for its improved usability - theme - Technology Evaluation - 2013"
  },
  {
    number: 33,
    cite: "Member (Opponent) of the PhD judging committee for Nauman bin Ali Supervisors: main supervisor Claes Wohlin and Kai Petersen, BTH, theme Software Engineering - June 2015"
  },
  {
    number: 34,
    cite: "Co-supervisor for Kristoffer Hagen, main supervisor Alf Inge Wang, theme - Entertainment Computing (exergames)"
  },
  {
    number: 35,
    cite: <p>Co-supervisor for <a href="https://scholar.google.no/citations?user=VVzDbqEAAAAJ&hl=en">Sofia Papavlasopoulou</a> with Michail Giannakos, theme Creativity - 2015 - 2019 </p>
  },
  {
    number: 36,
    cite: "Member (Opponent) of the PhD Judging committee for Raffaello Brondi, Scuola Superiore Sant’Anna, Italym Supervisor Marcello Carrozzino – theme Technology Enhanced Learning, November 2015"
  },
  {
    number: 37,
    cite: "Supervisor for Dr. Ilias Pappas with Michail Giannakos –  ERCIM 2016 – project EU Horizon 2020 Socratic 2017 project EU Initiate, theme Social Innovation"
  },
  {
    number: 38,
    cite: "Member of the PhD judging committee for Mohamed Abbadi, University of Venezia, Casanova 2, supervisor Agostino Cortesi, theme - Computer Games - 2016"
  },
  {
    number: 39,
    cite: "Supervisor for Dr. Simone Mora – 2017 EU Horizon 2020 Socratic"
  },
  {
    number: 40,
    cite: "Co-supervisor for Katerina Mangaroska, main Supervisor Michail Giannakos project NFR Future Learning"
  },
  {
    number: 41,
    cite: "Co-supervisor for Per Håkon Meland, main Supervisor Guttorm Sindre, theme Software Security"
  },
  {
    number: 42,
    cite: "Co-supervisor for Madeleine Aurora Lorås with Trond Aalberg and Guttorm Sindre, theme - Computing Education - 2017 - 2021"
  },
  {
    number: 43,
    cite: "Supervisor for ERCIM postdoc Javier Gomez Escribano, theme - Technology Enhanced Learning for Disability - 2018"
  },
  {
    number: 44,
    cite: "Co-supervisor for Justyna Szynkiewicz, main supervisor Line Kolås, theme - IT Education"
  },
  {
    number: 45,
    cite: "Member of the committee for Deepika Badampudi, supervisor Claes Wohlin, BTH, theme - Software Engineering, May 2018"
  },
  {
    number: 46,
    cite: "Supervisor for Orges Cico, project IPIT, theme - Software Engineering, 2018 - now"
  },
  {
    number: 47,
    cite: "Co-supervisor for Leif E. Opland, theme - Digital Innovation, 2018 – now"
  },
  {
    number: 48,
    cite: "Supervisor for ERCIM postdoc  Juan Carlos Torrado, theme - Health Technology, Story Telling - 2019"
  },
  {
    number: 49,
    cite: "Supervisor for PhD Farzana Quayyum, theme Children, Security, Gamification – 2019 – now"
  },
  {
    number: 50,
    cite: "Opponent for Mariasole Bondioli, Universita’ di Pisa – Developing Technological solutions to assist children with ASD with application to real life and diagnostic scenarios, supervisors Susanna Pelagatti and Stefano Chessa - 2020"
  },
  {
    number: 51,
    cite: "Co-supervisor for Jakob Notland, supervisor Jingyue Li, co-supervisor Mariusz Nowostawski, theme – Blockchains"
  },
  {
    number: 52,
    cite: "Co-supervisor for Saumitra Dwivedi, supervisor Ricardo Da Silva Torres; Anniken Susanne T. Karlsen ; Ibrahim A. Hameed – theme – Data Driven Modeling (only first year)"
  },
  {
    number: 53,
    cite: "Opponent for Hugo Sica de Andrade, supervisor Ivica Crnkovic, Chalmers University of Technology, theme - Software Engineering"
  },
  {
    number: 54,
    cite: "Supervisor for PhD Marte Hoff Hagen, theme - Health Technology, Children, Interaction Design – 2021 – now"
  },
  {
    number: 55,
    cite: "Supervisor for ERCIM Dr. J. David Paton-Romero, theme - Sustainability, Software Engineering - 2021 - 2022"
  },
  {
    number: 54,
    cite: "Supervisor for PhD Ibrahim El Shemy, theme - Health Technology, Children – 2021 – now"
  },
  { 
    number: 55,
    cite: "Supervisor for Postdoctoral Research Dr. Alicia Julia Wilson Takaoka, theme - Informatics, Climate Neutrality, Marginalized Women, and Access to Technology - 2022 – now"
  },
  {
    number: 56,
    cite: "Supervisor for Postdoctoral Research Dr. Anna Szlavi, theme - Gender balance in Informatics - 2022-now"
  },
  {
    number: 57,
    cite: "Supervisor for PhD Claudia Maria Cutrupi, theme - Diversity, Computer Science - 2022 - now"
  }
]

export const citings = cites.reverse()
