import styled from "styled-components"

// Icons wrapper
export const TopicWrapper = styled.div`
  width: inherit;
  display: grid;
  grid-gap: 1rem;
  grid-template-columns: repeat(auto-fill, minmax(160px, 1fr));
  `

// Homepage icons
export const Topic = styled.a`
  display: flex;
  flex-direction: column;
  align-items: center;
  position: relative;
  cursor: pointer;
  color: white;
  margin: 0.2rem;
  padding: 0.4rem;
  border-radius: 8px;
  background-color: ${({back}) => back ? back : 'tranparent'};
  text-align: center;
  min-height: 178px;
  justify-content: center;
  & p { text-align: center; }
  & img {
    width: 60px;
    height: 60px;
    object-fit: contain;
    margin: 10px 0;
  }
  &:hover {
    color: #ffffffdb;
  }
`