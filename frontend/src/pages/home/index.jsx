import React          from 'react'
import Header         from './Header'
import { Container }  from '../../components/globals'

export default function Home() {
  return <Container><Header/></Container>
}