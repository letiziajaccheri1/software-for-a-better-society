import React from "react";
import { Subheading } from '../../components/globals';

export const MasterSetup = (
  <>
    <Subheading>Theses supervised by Letizia Jaccheri at NTNU</Subheading>
    <p> See <a href="https://ntnuopen.ntnu.no/ntnu-xmlui/discover?rpp=10&etal=0&query=jaccheri&group_by=none&page=4&filtertype_0=advisor&filter_relational_operator_0=contains&filter_0=Jaccheri" target="_blank" rel="noopener noreferrer">in NTNU Open</a></p>
  </>
)
