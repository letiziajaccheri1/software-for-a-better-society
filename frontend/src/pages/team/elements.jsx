import styled from "styled-components";
import { Row } from "../../components/globals";
export const Members = styled.div`
  width: 100%;
  height: auto;
  border: none;
  padding: 10px 18px;
  margin: 10px 0;
  border-radius: 4px;
  & p { 
    margin-bottom: 8px; 
    font-weight: 500;
  }
  & h4 {
    display: inline-block;
    padding: 5px 10px;
    margin-bottom: 10px;
    color: white;
    background-color: #000000d4;
  }
`

export const PersonContainer = styled.div`
  display: flex;
  width: 100%;
  & img {
    width: 40px;
    height: auto;
    margin: 10px;
  }
`

export const Person = styled.div`
  position: relative;
  padding: 10px;
  margin: 10px 0;
  width: 100%;
  display: grid;
  background-color: #ffebda;
  & small {
    display: inline;
  }
  & img {
    cursor: pointer;
    width: 25px;
    height: auto;
  }
`

export const SocialMedia = styled(Row)`
  justify-content: initial;
  margin-top: 10px;
  & img {
    margin: 0 1rem;
  }
  & img:first-child { margin-left: 0; }
`