import styled from 'styled-components'
export const CooperatorsContainer = styled.div`
  /* display: flex;
  flex-wrap: wrap;
  justify-content: center; */
  display: grid;
  gap: 1rem;
  grid-template-columns: repeat(3, 1fr);
  padding: 2rem 0;
  width: 100%;
  max-width: 990px;

  @media only screen and (max-width: 820px) {
    grid-template-columns: repeat(2, 1fr); 
  }

  @media only screen and (max-width: 558px) {
    grid-template-columns: repeat(1, 1fr); 
  }
`

export const CooperatorCard = styled.div`
  display: flex;
  flex-grow: 1;
  flex-shrink: 1;
  flex-basis: 0;
  justify-content: center;
  align-items: center;
  width: 100%;
/*   max-width: 400px;
  min-width: 150px; */
  aspect-ratio: 2/1;
  border: solid 0.5px lightgray;
  padding: 1rem;

  & img, svg {
    -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
    filter: grayscale(100%);
    width: 220px;
    height: 60px;
    object-fit: contain;
  }

  & img:hover, svg:hover {
    -webkit-filter: grayscale(0%); /* Safari 6.0 - 9.0 */
    filter: grayscale(0%);
  }
`