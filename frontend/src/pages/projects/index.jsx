import React from 'react';
import { ProjectsLink } from './elements'
import { Wrapper, Row, H1, CustomWidthWrapper, CustomContainer } from '../../components/globals';
import { Navigation } from '../../components/globals/Navigation';

// Images for projects
import active   from  "./static/active.jpg";
import completed from  "./static/letizia.jpg";


export default function Projects() {
  return (
    <Wrapper>
      <Row>
        <CustomWidthWrapper>
          <Navigation><a href="/">home</a>&#47;<a href="/projects">projects</a></Navigation>
          <CustomContainer>
            <H1>Projects</H1>
            <Row>
              <ProjectsLink href='/active-projects'><img src={active} alt="active-projects"/><h4>Active Projects</h4></ProjectsLink>
              <ProjectsLink href='/completed-projects'><img src={completed} alt="completed-projects"/><h4>Completed Projects</h4></ProjectsLink>
            </Row>
          </CustomContainer>
        </CustomWidthWrapper>
      </Row>
    </Wrapper>
  )
}